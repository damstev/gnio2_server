from django.conf.urls import patterns, include, url
from rest_framework import routers

from location import views

router = routers.DefaultRouter()
router.register(r'api/countries', views.CountryViewSet)
router.register(r'api/regions', views.RegionViewSet)
router.register(r'api/cities', views.CityViewSet)

urlpatterns = patterns('',
                       url(r'^', include(router.urls)),
                       )
