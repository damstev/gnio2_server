from cities_light.models import Country, Region, City
from rest_framework import viewsets
from .serializers import CountrySerializer, RegionSerializer, CitySerializer


class CountryViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class RegionViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    #filter_fields = ('country',)
    #paginate_by = 100
    #lookup_field = 'name'

    def get_queryset(self):
        queryset = self.queryset
        country = self.request.QUERY_PARAMS.get('country', None)
        if country is not None:
            queryset = queryset.filter(country=country)
        return queryset


class CityViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = City.objects.all()
    serializer_class = CitySerializer

    def get_queryset(self):
        queryset = self.queryset
        region = self.request.QUERY_PARAMS.get('region', None)
        if region is not None:
            queryset = queryset.filter(region=region)
        return queryset
