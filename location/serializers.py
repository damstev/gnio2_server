from rest_framework import serializers
from cities_light.models import Country, Region, City


class CountrySerializer(serializers.ModelSerializer):

    class Meta:
        model = Country
        fields = ('id', 'name')


class RegionSerializer(serializers.ModelSerializer):
    #country = serializers.StringRelatedField()
    #country = CountrySerializer(read_only=True)
    country = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='country-detail'
    )

    class Meta:
        model = Region
        fields = ('id', 'name', 'country')


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ('id', 'name', 'region')
