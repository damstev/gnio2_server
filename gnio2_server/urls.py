from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
                       url(r'^api/', include('quickstart.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^location/', include('location.urls')),
                       url(r'^collecting/', include('collecting.urls')),
                       url(r'^devices/', include('devices.urls')),
                       )
