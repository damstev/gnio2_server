from rest_framework import viewsets
from .serializers import UserCollectedSerializer
from .models import UserCollected


class UserCollectedViewSet(viewsets.ModelViewSet):

    queryset = UserCollected.objects.all()
    serializer_class = UserCollectedSerializer
