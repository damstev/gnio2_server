from django.db import models
from cities_light.models import Country, Region, City


class Sector(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


class BusinessType(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name


# Create your models here.
class UserCollected(models.Model):
    name = models.CharField(max_length=254)
    email = models.EmailField(max_length=254)
    company = models.CharField(max_length=254)
    phone = models.CharField(max_length=25)
    celphone = models.CharField(max_length=25)
    whatsapp = models.CharField(max_length=25, null=True, blank=True)
    bbm = models.CharField(max_length=25, null=True, blank=True)
    address = models.CharField(max_length=254, null=True, blank=True)

    business_type = models.ForeignKey(
        BusinessType, related_name="users", null=True, blank=True)
    sector = models.ForeignKey(
        Sector, related_name="users", null=True, blank=True)
    country = models.ForeignKey(Country, related_name="users")
    region = models.ForeignKey(Region, related_name="users")
    city = models.ForeignKey(City, related_name="users", null=True, blank=True)

    def __unicode__(self):
        return self.name
