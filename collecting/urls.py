from django.conf.urls import patterns, include, url
from rest_framework import routers

from collecting import views

router = routers.DefaultRouter()
router.register(r'api/usercollected', views.UserCollectedViewSet)

urlpatterns = patterns('',
                       url(r'^', include(router.urls)),
                       )
