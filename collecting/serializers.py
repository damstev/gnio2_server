from rest_framework import serializers
from .models import UserCollected


class UserCollectedSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserCollected
