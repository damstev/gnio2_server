from django.contrib import admin
from .models import Sector, BusinessType, UserCollected


@admin.register(Sector, BusinessType, UserCollected)
class SectorAdmin(admin.ModelAdmin):
    pass
