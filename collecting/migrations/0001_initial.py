# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cities_light', '0003_auto_20141120_0342'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusinessType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserCollected',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=254)),
                ('email', models.EmailField(max_length=254)),
                ('company', models.CharField(max_length=254)),
                ('phone', models.CharField(max_length=25)),
                ('celphone', models.CharField(max_length=25)),
                ('whatsapp', models.CharField(max_length=25)),
                ('bbm', models.CharField(max_length=25)),
                ('address', models.CharField(max_length=254)),
                ('business_type', models.ForeignKey(related_name='users', to='collecting.BusinessType')),
                ('city', models.ForeignKey(related_name='users', to='cities_light.City')),
                ('country', models.ForeignKey(related_name='users', to='cities_light.Country')),
                ('region', models.ForeignKey(related_name='users', to='cities_light.Region')),
                ('sector', models.ForeignKey(related_name='users', to='collecting.Sector')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
