# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collecting', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usercollected',
            name='address',
            field=models.CharField(max_length=254, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usercollected',
            name='bbm',
            field=models.CharField(max_length=25, null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usercollected',
            name='whatsapp',
            field=models.CharField(max_length=25, null=True),
            preserve_default=True,
        ),
    ]
