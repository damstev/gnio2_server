# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collecting', '0003_auto_20150103_1808'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usercollected',
            name='business_type',
            field=models.ForeignKey(related_name='users', blank=True, to='collecting.BusinessType', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='usercollected',
            name='sector',
            field=models.ForeignKey(related_name='users', blank=True, to='collecting.Sector', null=True),
            preserve_default=True,
        ),
    ]
