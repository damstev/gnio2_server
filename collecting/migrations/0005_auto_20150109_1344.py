# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('collecting', '0004_auto_20150108_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usercollected',
            name='city',
            field=models.ForeignKey(related_name='users', blank=True, to='cities_light.City', null=True),
            preserve_default=True,
        ),
    ]
