from django.conf.urls import patterns, include, url
from rest_framework import routers

from devices import views

router = routers.DefaultRouter()
router.register(r'api/brands', views.BrandViewSet)
router.register(r'api/models', views.ModelViewSet)
router.register(r'api/devices', views.DeviceViewSet)

urlpatterns = patterns('',
                       url(r'^', include(router.urls)),
                       )
