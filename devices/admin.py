from django.contrib import admin
from .models import Brand, Model, Device


@admin.register(Brand, Model, Device)
class BrandAdmin(admin.ModelAdmin):
    pass
