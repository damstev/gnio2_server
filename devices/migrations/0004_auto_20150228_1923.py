# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0003_auto_20150228_1850'),
    ]

    operations = [
        migrations.AddField(
            model_name='device',
            name='dev_brand',
            field=models.ForeignKey(related_name='devices', blank=True, to='devices.Brand', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='device',
            name='dev_model',
            field=models.ForeignKey(related_name='devices', blank=True, to='devices.Model', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='device',
            name='user_address',
            field=models.CharField(max_length=254, null=True),
            preserve_default=True,
        ),
    ]
