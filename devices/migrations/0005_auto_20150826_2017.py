# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0004_auto_20150228_1923'),
    ]

    operations = [
        migrations.AddField(
            model_name='model',
            name='extra',
            field=models.TextField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='device',
            name='dev_imei',
            field=models.CharField(max_length=15, unique_for_date=b'updated_at'),
            preserve_default=True,
        ),
    ]
