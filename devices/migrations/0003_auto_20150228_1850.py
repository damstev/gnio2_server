# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('devices', '0002_device'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='dev_charger',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
