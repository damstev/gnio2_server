# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cities_light', '0003_auto_20141120_0342'),
        ('devices', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Device',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dev_imei', models.CharField(max_length=15, unique_for_date=b'update_at')),
                ('dev_status', models.PositiveSmallIntegerField(choices=[(1, b'Excelente'), (2, b'Bueno'), (3, b'Regular')])),
                ('dev_charger', models.BooleanField()),
                ('dev_img_id', models.CharField(max_length=254)),
                ('user_first_name', models.CharField(max_length=254)),
                ('user_last_name', models.CharField(max_length=254)),
                ('user_email', models.EmailField(max_length=254)),
                ('user_phone', models.CharField(max_length=25)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('user_city', models.ForeignKey(related_name='devices', to='cities_light.City')),
                ('user_country', models.ForeignKey(related_name='devices', to='cities_light.Country')),
                ('user_region', models.ForeignKey(related_name='devices', to='cities_light.Region')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
