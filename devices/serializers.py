from rest_framework import serializers
from devices.models import Brand, Model, Device


class BrandSerializer(serializers.ModelSerializer):

    class Meta:
        model = Brand
        fields = ('id', 'name')


class ModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Model
        fields = ('id', 'name', 'brand', 'extra')

class DeviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Device
