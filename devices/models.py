from django.db import models
from cities_light.models import Country, Region, City

# Create your models here.
class Brand(models.Model):
    name = models.CharField(max_length=254, unique=True)
    status = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Model(models.Model):
    name = models.CharField(max_length=254, unique=True)
    status = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    extra = models.TextField(null=True)

    brand = models.ForeignKey(
        Brand, related_name='models', null=True, blank=True)

    def __unicode__(self):
        return self.name


# Create your models here.
class Device(models.Model):
    DEVICE_STATUSES = (
        (1, 'Excelente'),
        (2, 'Bueno'),
        (3, 'Regular'),
    )

    dev_imei = models.CharField(max_length=15, unique_for_date="updated_at")
    dev_status = models.PositiveSmallIntegerField(choices=DEVICE_STATUSES)
    dev_charger = models.BooleanField(default=False)
    dev_img_id = models.CharField(max_length=254)

    user_first_name = models.CharField(max_length=254)
    user_last_name = models.CharField(max_length=254)
    user_email = models.EmailField(max_length=254)
    user_phone = models.CharField(max_length=25)
    user_country = models.ForeignKey(Country, related_name="devices")
    user_region = models.ForeignKey(Region, related_name="devices")
    user_city = models.ForeignKey(City, related_name="devices")
    user_address = models.CharField(max_length=254, null=True)

    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    dev_brand = models.ForeignKey(
        Brand, related_name='devices', null=True, blank=True)

    dev_model = models.ForeignKey(
        Model, related_name='devices', null=True, blank=True)

    def __unicode__(self):
        return self.dev_imei
