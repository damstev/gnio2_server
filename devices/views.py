from rest_framework import viewsets
from devices.serializers import BrandSerializer, ModelSerializer, DeviceSerializer
from .models import Model, Brand, Device


class BrandViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Brand.objects.all()
    serializer_class = BrandSerializer


class ModelViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Model.objects.all()
    serializer_class = ModelSerializer

    def get_queryset(self):
        queryset = self.queryset
        brand = self.request.QUERY_PARAMS.get('brand', None)
        if brand is not None:
            queryset = queryset.filter(brand=brand)
        return queryset

class DeviceViewSet(viewsets.ModelViewSet):

    queryset = Device.objects.all()
    serializer_class = DeviceSerializer